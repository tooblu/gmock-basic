#+title: Googletest/mock MACHINA

* Googletest in a project
** Project level
   #+begin_src
     filewriter
     |-- CMakeLists.txt
     |-- shell.nix
     |-- src
     |   |-- AppExample.cpp
     |   |-- CMakeLists.txt
     |   |-- FileSystem.h
     |   `-- FileWriter.h
     `-- test
         |-- CMakeLists.txt
         |-- FakeFileSystem.h
         `-- FileWriterTests.cpp
   #+end_src
** File level
   - one file per class
** Test level
   - one test per method is not a bad place to start
** Building
   - mostly just adding an executable
   #+begin_src cmake
     add_executable(
       tests
       FileWriterTests.cpp
       )
   #+end_src
** Running
   #+begin_src sh
     buld/test/tests
   #+end_src
* Googletest googletest
  https://google.github.io/googletest/
** Assertions
   #+begin_src c++
     EXPECT_EQ(a, b);
     EXPECT_TRUE(a);
     EXPECT_FALSE(a);
     EXPECT_NO_THROW(fn);
     EXPECT_THROW(fn, ex);
     EXPECT_THROW_ANY(fn);
     EXPECT_DOUBLE_EQ(a, b);
     EXPECT_NEAR(a, b, abs_error)
     // from gmock
     EXPECT_THAT(v, ::testing::StartsWith("abc"));
     EXPECT_THAT(v, ::testing::MatchesRegex("def"));
   #+end_src
** TEST
   #+begin_src c++
     #include "gtest/gtest.h"
     #include "FileSystem.h"
     #include "FileWriter.h"
     TEST(FileWriterTests, ctor)
     {
         FileSystem fs;
         EXPECT_NO_THROW(FileWriter fw(fs));
     }
   #+end_src
** TEST_F
   #+begin_src c++
     class FileWriterTests : public ::testing::Test
     {
         FileSystem fs;
     protected:
         FileWriterTests() {}
         ~FileWriterTests() override {}
         void SetUp() override {}
         void TearDown() override {}
     };
     TEST_F(FileWriterTests, ctor)
     {
         EXPECT_NO_THROW(FileWriter fw(fs));
     }
   #+end_src
** Run-time options
   #+begin_src sh
     buld/test/tests ...
     buld/test/tests --gtest_filter=ctor
     --gtest_filter=ctor
     --gtest_filter=FileWriter*:...
     --gtest_brief
     --gtest_repeat=10
     --gtest_shffule
   #+end_src
* Mocking
** When/Why
  - is difficult/time-consuming/expensive to access
  - is difficult/time-consuming/expensive to bring up
  - may not yet exist, i.e. during development
  - returns inconsistent results
  - is unreliable
** Examples
  - File system
  - Network
  - Database
  - On-line Service (Website, Search Engine, Dictionary, ...)
** Dependency injection
  - class does not init own dependencies
  - dependencies are provided externally
    - construction
    - method
  - mock dependency, not the class being tested
* Googlemock basic example
** Example description
   - Our flakey resource: filesystem
   - File writing library
** More examples
   #+begin_src c++
     EXPECT_CALL(mymockedobj, meth00())
         .Times(137)
         .WillOnce(Return(0.0))
         .WillOnce(Return(1.618))
         .WillRepeatedly(Return(6.283));
     EXPECT_CALL(mymockedobj, meth01())
         .Times(AtLeast(2));
   #+end_src
