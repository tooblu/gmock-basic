#include <iostream>

#include "FileSystem.h"
#include "FileWriter.h"

int main()
{
    FileSystem fs;
    FileWriter fw(fs);
    fw.write("filename", "text for to put in filename");
    return 0;
}
