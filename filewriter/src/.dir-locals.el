((nil
  . ((compile-command
      . (string-join
         (list
          "NIX_PATH=/home/jra/.nix-defexpr/channels ~/.nix-profile/bin/nix-shell"
          (concat (projectile-project-root) "shell.nix")
          "--pure"
          "--run"
          "'"
          "cmake"
          "--build" (concat (projectile-project-root) "build")
          "'"

          )
         " ")))))
