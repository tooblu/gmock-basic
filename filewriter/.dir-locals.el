((semantic-format-tag-canonical-name-c-mode . ((flycheck-clang-language-standard . "c++11"))))

((nil
  . ((compile-command
      . (string-join
         (list
          "nix-shell"
          (concat (projectile-project-root) "shell.nix")
          "--pure"
          "--run"
          "'"
          "cmake"
          "--build" (concat (projectile-project-root) "build")
          "'"

          )
         " ")))))
