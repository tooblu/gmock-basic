#include "gtest/gtest.h"

#include "FileSystem.h"
#include "FileWriter.h"

TEST(FileWriterTests, ctor)
{
    FileSystem fs;
    EXPECT_NO_THROW(FileWriter fw(fs));
}

//#include <filesystem>
//#include <fstream>
//
//TEST(FileWriterTests, write_bad)
//{
//    FileSystem fs;
//    FileWriter fw(fs);
//    const std::filesystem::path filename = std::filesystem::temp_directory_path() / "hopethisfnisntused";
//    const std::string contents = "text for to put in filename";
//    fw.write(filename.string(), contents);
//    std::filesystem::path filename_txt = filename;
//    filename_txt.replace_extension(std::filesystem::path("txt"));
//    ASSERT_TRUE(std::filesystem::exists(filename_txt));
//    std::ifstream ifs(filename_txt);
//    std::string contents_test (
//        (std::istreambuf_iterator<char>(ifs)),
//        (std::istreambuf_iterator<char>()));
//    ifs.close();
//    EXPECT_EQ(contents, contents_test);
//    EXPECT_NO_THROW(std::filesystem::remove(filename));
//}

#include "gmock/gmock.h"
#include "FakeFileSystem.h"

using ::testing::_;
using ::testing::Return;

TEST(FileWriterTests, write_good)
{
    FakeFileSystem fs;
    FileWriter fw(fs);
    // only checking for the call
    EXPECT_CALL(fs, write_to_storage(_, _)).Times(1);
    fw.write("filename", "text for to put in filename");
}

TEST(FileWriterTests, write_bitbetter)
{
    FakeFileSystem fs;
    FileWriter fw(fs);
    const std::string fn = "filename";
    // simple matcher: call and correct arg
    EXPECT_CALL(fs, write_to_storage(fn + ".txt", _)).Times(1);
    fw.write(fn, "text for to put in filename");
}

TEST(FileWriterTests, write_better)
{
    FakeFileSystem fs;
    FileWriter fw(fs);
    const std::string fn = "filename";
    const std::string ct = "text for to put in filename";
    // simple matcher: call and correct arg
    EXPECT_CALL(fs, write_to_storage(fn + ".txt", _)).Times(1).WillOnce(Return(ct.size ()));
    //EXPECT_CALL(fs, write_to_storage(fn + ".txt", _)).Times(1).WillOnce(Return(2000000));
    //EXPECT_CALL(fs, write_to_storage(fn + ".txt", _)).WillOnce(Return(2000000));
    EXPECT_NO_THROW(fw.write_check(fn, ct));
}

TEST(FileWriterTests, write_better_fail)
{
    FakeFileSystem fs;
    FileWriter fw(fs);
    const std::string fn = "filename";
    const std::string ct = "text for to put in filename";
    // simple matcher: call and correct arg
    EXPECT_CALL(fs, write_to_storage(fn + ".txt", _)).Times(1).WillOnce(Return(ct.size () + 1));
    //EXPECT_CALL(fs, write_to_storage(fn + ".txt", _)).Times(1).WillOnce(Return(2000000));
    //EXPECT_CALL(fs, write_to_storage(fn + ".txt", _)).WillOnce(Return(2000000));
    EXPECT_ANY_THROW(fw.write_check(fn, ct));
}
